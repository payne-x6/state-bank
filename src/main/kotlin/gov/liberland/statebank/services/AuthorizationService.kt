package gov.liberland.statebank.services

import gov.liberland.statebank.utils.security.Authorization
import org.springframework.security.authentication.AnonymousAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

@Service
class AuthorizationService {

    val isLoggedIn : Boolean
        get() = SecurityContextHolder.getContext().authentication !is AnonymousAuthenticationToken

    val hasRoleUser : Boolean
        get() = SecurityContextHolder.getContext().authentication.authorities.find { it.authority == "ROLE_USER" } != null

    val hasRoleAdmin : Boolean
        get() = SecurityContextHolder.getContext().authentication.authorities.find { it.authority == "ROLE_ADMIN" } != null

    val hasRoleSuperUser : Boolean
        get() = SecurityContextHolder.getContext().authentication.authorities.find { it.authority == "ROLE_SUPERADMIN" } != null


    fun getAuthorizationToken() : Authorization {
        return Authorization(!isLoggedIn, hasRoleUser, hasRoleAdmin, hasRoleSuperUser)
    }

}