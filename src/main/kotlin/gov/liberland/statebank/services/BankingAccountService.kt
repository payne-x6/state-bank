package gov.liberland.statebank.services

import gov.liberland.statebank.forms.SignupForm
import gov.liberland.statebank.model.repository.AccountRepository
import gov.liberland.statebank.model.repository.RoleRepository
import gov.liberland.statebank.model.repository.UserRepository
import gov.liberland.statebank.model.table.Account
import gov.liberland.statebank.model.table.User
import javassist.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

@Service
class BankingAccountService {

    @Autowired
    private lateinit var userService : UserService

    @Autowired
    private lateinit var accountRepository : AccountRepository

    fun getBankingAccounts() : List<Account> {
        return accountRepository.findAllByUser(userService.getThisUser())
    }


}