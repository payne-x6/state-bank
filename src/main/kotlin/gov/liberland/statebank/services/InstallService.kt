package gov.liberland.statebank.services

import gov.liberland.statebank.model.repository.RoleRepository
import gov.liberland.statebank.model.repository.UserRepository
import gov.liberland.statebank.model.table.Role
import gov.liberland.statebank.model.table.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service
class InstallService {

    @Autowired
    private lateinit var roleRepository: RoleRepository

    @Autowired
    private lateinit var userRepository: UserRepository


    val isInstalled : Boolean
        get() {
            val superusersByRole = roleRepository.findByRole("SUPERADMIN")
            return (superusersByRole?.users?.firstOrNull() != null)
        }

    fun initRoles() {
        if(!isInstalled) {
            val roles = listOf(Role(role = "USER"), Role(role = "ADMIN"), Role(role = "SUPERADMIN"))
            roleRepository.saveAll(roles)
        }
    }

    fun addSuperuser(password : String) {
        val roleSuperAdmin = roleRepository.findByRole("SUPERADMIN")
        if(roleSuperAdmin != null) {
            val user = User(
                    name = "Liberland Statebank Superuser",
                    email = "support@statebank.ll",
                    birthDate = LocalDate.now(),
                    city = "Liberland",
                    country = "Liberland",
                    state = "Liberland",
                    streetAddress = "Liberland 1",
                    username = "root",
                    password = BCryptPasswordEncoder().encode(password),
                    roles = mutableListOf(roleSuperAdmin)
            )
            userRepository.save(user)
        }

    }
}