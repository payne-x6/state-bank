package gov.liberland.statebank.services

import gov.liberland.statebank.forms.SignupForm
import gov.liberland.statebank.model.repository.RoleRepository
import gov.liberland.statebank.model.repository.UserRepository
import gov.liberland.statebank.model.table.User
import javassist.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserService {
    @Autowired
    private lateinit var authorizationService: AuthorizationService

    @Autowired
    private lateinit var userRepository : UserRepository

    @Autowired
    private lateinit var roleRepository: RoleRepository

    fun registerUser(form : SignupForm) {
        if(form.isSuccessful) {
            var roles = roleRepository.findAllByRoleIn(form.roles)
            roles = if (authorizationService.hasRoleSuperUser) {
                        roles.filter { it.role != "SUPERADMIN" }
                    }
                    else {
                        roles.filter { it.role != "ADMIN" || it.role != "SUPERADMIN" }
                    }
            val user = User(
                    name = form.name,
                    email = form.email,
                    birthDate = form.birthDate,
                    city = form.city,
                    country = form.country,
                    postalCode = form.postalCode,
                    state = form.state,
                    streetAddress = form.streetAddress,
                    title = form.title,
                    username = form.username,
                    password = BCryptPasswordEncoder().encode(form.password),
                    phoneNumber = form.phoneNumber,
                    roles = roles
            )

            userRepository.save(user)
        }
    }

    fun countUsers() : Long {
        return userRepository.count()
    }

    fun countAdmins() : Long {
        var roles = roleRepository.findAllByRoleIn(listOf("ADMIN", "SUPERADMIN"))
        return userRepository.countByRolesIn(roles)
    }

    fun getThisUser() : User {
        val username = SecurityContextHolder.getContext().authentication.name
        return userRepository.findByUsername(username) ?: throw NotFoundException("Username not found")
    }
}