package gov.liberland.statebank.services

import gov.liberland.statebank.model.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import java.lang.Exception

@Service
class AuthenticationService : UserDetailsService {

    @Autowired
    private lateinit var userRepository : UserRepository


    @Throws(NoSuchElementException::class, Exception::class)
    override fun loadUserByUsername(username: String): UserDetails {
        val user = userRepository.findByUsername(username) ?: throw UsernameNotFoundException("Wrong username or password")
        return User(user.username, user.password, user.roles.map { SimpleGrantedAuthority( "ROLE_${it.role}" )}.toSet())
    }
}