package gov.liberland.statebank.model.repository

import gov.liberland.statebank.model.table.Role
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Transactional
@Repository
interface RoleRepository : JpaRepository<Role, Long> {

    fun findByRole(role : String) : Role?

    fun findAllByRoleIn(roles : List<String>) : List<Role>

}