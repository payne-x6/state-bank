package gov.liberland.statebank.model.repository

import gov.liberland.statebank.model.table.User
import gov.liberland.statebank.model.table.Account
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Transactional
@Repository
interface AccountRepository : JpaRepository<Account, Long> {

    fun findAllByUser(user : User) : List<Account>
}