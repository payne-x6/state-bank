package gov.liberland.statebank.model.repository

import gov.liberland.statebank.model.table.Role
import gov.liberland.statebank.model.table.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Transactional
@Repository
interface UserRepository : JpaRepository<User, Long> {

    fun findByUsername(username : String) : User?

    fun findAllByRolesIn(roles : List<Role>) : List<User>

    fun countByRolesIn(roles : List<Role>) : Long


}