package gov.liberland.statebank.model.table

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.hibernate.annotations.Generated
import org.hibernate.annotations.GenerationTime
import javax.persistence.*

@Entity
@Table(name="ACCOUNTS")
data class Account(
        @Id @GeneratedValue(strategy= GenerationType.AUTO) @Column(name="account_id") var accountId : Long = 0,
        @Column(nullable = false) var name : String,
        @Column(nullable = false) var transparent : Boolean,

        @ManyToOne(fetch = FetchType.LAZY)
        @JsonIgnoreProperties("accounts")
        var user : User,

        @OneToMany(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
        var outcomes : List<Transaction> = mutableListOf(),

        @OneToMany(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
        var incomes : List<Transaction> = mutableListOf()
)
{
        val accountNumber : String
                get() = "%016d/2015".format(accountId)
}
