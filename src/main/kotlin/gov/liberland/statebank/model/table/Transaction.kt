package gov.liberland.statebank.model.table

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.hibernate.annotations.Generated
import org.hibernate.annotations.GenerationTime
import javax.persistence.*

@Entity
@Table(name="TRANSACTIONS")
data class Transaction(
        @Id @GeneratedValue(strategy= GenerationType.AUTO) @Column(name="transaction_id") var transactionId : Long = 0,
        @Column(nullable = false) var amount : Double,

        @ManyToOne(fetch = FetchType.LAZY)
        var sender : Account,

        @ManyToOne(fetch = FetchType.LAZY)
        var payee : Account
)
