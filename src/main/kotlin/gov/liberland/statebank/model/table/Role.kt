package gov.liberland.statebank.model.table

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import javax.persistence.*

@Entity
@Table(name="ROLES")
data class Role(
        @Id @GeneratedValue(strategy=GenerationType.AUTO) @Column(name="role_id") var roleId : Long = 0,
        @Column(nullable = false, unique = true) var role : String,

        @ManyToMany(fetch = FetchType.EAGER, cascade = [CascadeType.PERSIST, CascadeType.MERGE], mappedBy = "roles")
        @JsonIgnoreProperties("roles")
        var users : List<User> = mutableListOf()
)
