package gov.liberland.statebank.model.table

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name="USERS")
data class User(
        @Id @GeneratedValue(strategy= GenerationType.AUTO) @Column(name="user_id") var userId : Long = 0,
        var title : String? = null,
        @Column(nullable = false) var name : String,
        @Column(nullable = false) var birthDate : LocalDate,
        @Column(nullable = false, unique = true) var email : String,
        @Column(unique = true) var phoneNumber : String? = null,
        @Column(nullable = false) var streetAddress : String,
        @Column(nullable = false) var city : String,
        @Column(nullable = false) var state : String,
        var postalCode : String? = null,
        @Column(nullable = false) var country : String,
        @Column(nullable = false, unique = true) var username : String,
        @Column(nullable = false) var password : String,



        @ManyToMany(fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
        @JoinTable(
                name = "USER_ROLE",
                joinColumns = [JoinColumn(name="user_id")],
                inverseJoinColumns = [JoinColumn(name = "role_id")]
        )
        @JsonIgnoreProperties("users")
        var roles : List<Role> = mutableListOf(),

        @OneToMany(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
        @JsonIgnoreProperties("user")
        var accounts : List<Account> = mutableListOf()

)
