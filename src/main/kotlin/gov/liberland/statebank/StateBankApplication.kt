package gov.liberland.statebank

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class StateBankApplication

fun main(args: Array<String>) {
    runApplication<StateBankApplication>(*args)
}
