package gov.liberland.statebank.layouts

import com.samskivert.mustache.Mustache
import com.samskivert.mustache.Template
import java.io.IOException
import java.io.Writer

class BankingLayout(private val compiler: Mustache.Compiler) : Mustache.Lambda {

    lateinit var content: String

    @Throws(IOException::class)
    override fun execute(frag: Template.Fragment, out: Writer) {
        content = frag.execute()
        compiler.compile("{{>banking-template}}").execute(frag.context(), out)
    }

}