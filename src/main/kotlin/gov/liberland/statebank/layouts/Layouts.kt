package gov.liberland.statebank.layouts

import com.samskivert.mustache.Mustache
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.beans.factory.annotation.Autowired


@ControllerAdvice
class Layouts @Autowired constructor(private val compiler: Mustache.Compiler) {


    @ModelAttribute("public-template")
    fun publicLayout(model: Map<String, Any>): Mustache.Lambda {
        return PublicLayout(compiler)
    }

    @ModelAttribute("minimalistic-template")
    fun minimalisticLayout(model: Map<String, Any>): Mustache.Lambda {
        return MinimalisticLayout(compiler)
    }

    @ModelAttribute("banking-template")
    fun bankingLayout(model: Map<String, Any>): Mustache.Lambda {
        return BankingLayout(compiler)
    }

}

