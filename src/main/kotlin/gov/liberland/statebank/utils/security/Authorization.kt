package gov.liberland.statebank.utils.security


data class Authorization (
        val isAnonymous : Boolean,
        val isUser : Boolean,
        val isAdmin : Boolean,
        val isSuperAdmin : Boolean
)
{
    val hasAdminRole : Boolean
        get() = isAdmin || isSuperAdmin
}