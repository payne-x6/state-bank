package gov.liberland.statebank.utils

import java.util.regex.Pattern
import java.util.regex.Pattern.*


object Pattern {
    val PHONE : Pattern = compile("(\\+[0-9]+[\\- \\.]*)?(\\([0-9]+\\)[\\- \\.]*)?([0-9][0-9\\- \\.][0-9\\- \\.]+[0-9])")
    val EMAIL_ADDRESS : Pattern = compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+")
    val POSTAL_ZIP_CODE : Pattern = compile("[A-Z0-9][A-Z0-9 -]{0,8}[A-Z0-9]")
}
