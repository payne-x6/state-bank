package gov.liberland.statebank.utils

data class Alert(val message : String, val type : Type) {

    enum class Type(val string : String) {
        INFO("alert-primary"),
        SUCCESS("alert-success"),
        WARNING("alert-warning"),
        ERROR("alert-danger");

        override fun toString() : String {
            return string
        }
    }
}