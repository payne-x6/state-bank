package gov.liberland.statebank.utils

fun String?.fromRequestToBoolean(default : Boolean) : Boolean {
    return when(this) {
        "on" -> true
        "off" -> false
        else -> default
    }
}