package gov.liberland.statebank.utils

data class Navigation(val name : String, val href : String)