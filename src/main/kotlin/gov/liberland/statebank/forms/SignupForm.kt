package gov.liberland.statebank.forms

import gov.liberland.statebank.utils.Alert
import gov.liberland.statebank.utils.Pattern
import gov.liberland.statebank.utils.fromRequestToBoolean
import java.time.LocalDate

class SignupForm(title : String? = null, name : String, birthDate: LocalDate, email: String, phoneNumber: String? = null, streetAddress: String, city: String, state: String, postalCode: String? = null, country: String, username: String, password: String, passwordRepetition : String, role_user : Boolean = true , role_admin : Boolean = false) : Form() {

    val title : String?
    val name : String
    val birthDate : LocalDate
    val email : String
    val phoneNumber : String?
    val streetAddress : String
    val city : String
    val state : String
    val postalCode : String?
    val country : String
    val username : String
    val password : String
    val roles : List<String>


    constructor(map : Map<String, String>) : this(
            title = map["title"],
            name = map["name"].orEmpty(),
            birthDate = LocalDate.parse(map["birthDate"].orEmpty()),
            email = map["email"].orEmpty(),
            phoneNumber = map["phoneNumber"],
            streetAddress = map["streetAddress"].orEmpty(),
            city = map["city"].orEmpty(),
            state = map["state"].orEmpty(),
            postalCode = map["postalCode"],
            country = map["country"].orEmpty(),
            username = map["username"].orEmpty(),
            password = map["password"].orEmpty(),
            passwordRepetition = map["passwordRepetition"].orEmpty(),
            role_user = map["role_user"].fromRequestToBoolean(true),
            role_admin = map["role_admin"].fromRequestToBoolean(false)
            //role_admin = map["role_admin"]?.toBoolean() ?: false
    )

    init {
        //Validation
        //Title
        if(!title.isNullOrBlank()) {
            val title = title!!.trim()
            if (title.length in 1..127) {
                this.title = title
            } else {
                alerts.add(Alert("The title must be at most 127 characters long!", Alert.Type.WARNING))
                this.title = null
            }
        }
        else {
            this.title = null
        }

        //Name
        val name = name.trim()
        if(name.length in 3..255) {
            this.name = name
        }
        else {
            alerts.add(Alert("The name should be between 3 and 255 characters long!", Alert.Type.ERROR))
            this.name = ""
            isSuccessful = false
        }

        //Birth date
        if(birthDate.isBefore(LocalDate.now().minusYears(18)) && birthDate.isAfter(LocalDate.now().minusYears(200))) {
            this.birthDate = birthDate
        }
        else {
            alerts.add(Alert("You have to be over 18 years old!", Alert.Type.ERROR))
            this.birthDate = LocalDate.parse("")
            isSuccessful = false
        }

        //EMail
        val email = email
        if(Pattern.EMAIL_ADDRESS.matcher(email).matches()) {
            this.email = email
        }
        else {
            alerts.add(Alert("E-Mail is not in valid format!", Alert.Type.ERROR))
            this.email = ""
            isSuccessful = false
        }

        //Phone Number
        if(!phoneNumber.isNullOrBlank()) {
            val phoneNumber = phoneNumber!!.trim()
            if (phoneNumber.length in 0..24 && Pattern.PHONE.matcher(phoneNumber).matches() ) {
                this.phoneNumber = phoneNumber
            } else {
                alerts.add(Alert("Phone number has probably wrong format!\nWe will empty your phone number and you can choose one later in the Profile Settings.", Alert.Type.WARNING))
                this.phoneNumber = ""
            }
        }
        else {
            this.phoneNumber = null
        }

        //Street Address
        val streetAddress = streetAddress.trim()
        if(streetAddress.length in 3..255) {
            this.streetAddress = streetAddress
        }
        else {
            alerts.add(Alert("The street address should be between 3 and 255 characters long!", Alert.Type.ERROR))
            this.streetAddress = ""
            isSuccessful = false
        }

        //City
        val city = city.trim()
        if(city.length in 1..255) {
            this.city = city
        }
        else {
            alerts.add(Alert("The city is not in valid form!", Alert.Type.ERROR))
            this.city = ""
            isSuccessful = false
        }

        //State
        val state = state.trim()
        if(state.length in 2..128) {
            this.state = state
        }
        else {
            alerts.add(Alert("Fill up state!", Alert.Type.ERROR))
            this.state = ""
            isSuccessful = false
        }

        //Postal/Zip code
        if(!postalCode.isNullOrBlank()) {
            val postalCode = postalCode!!.trim()
            if (Pattern.POSTAL_ZIP_CODE.matcher(postalCode).matches()) {
                this.postalCode = postalCode
            } else {
                alerts.add(Alert("Insert the Postal / Zip code in proper format!", Alert.Type.WARNING))
                this.postalCode = ""
            }
        }
        else {
            this.postalCode = null
        }

        //Country
        val country = country.trim()
        if(country.length in 1..128) { //Generated country codes - can be size 1
            this.country = country
        }
        else {
            alerts.add(Alert("Fill up country!", Alert.Type.ERROR))
            this.country = ""
            isSuccessful = false
        }

        //Username
        val username = username.trim()
        if(username.length in 8..255) {
            this.username = username.trim()
        }
        else {
            alerts.add(Alert("Username should be at least 8 characters long", Alert.Type.ERROR))
            this.username = ""
            isSuccessful = false
        }

        //Roles
        val roles = mutableListOf<String>()
        if(role_user) {
            roles += "USER"
        }
        if(role_admin) {
            roles += "ADMIN"
        }
        if(roles.isEmpty()) {
            alerts.add(Alert("At least one role needed!", Alert.Type.ERROR))
            isSuccessful = false
        }
        this.roles = roles


        //Password comparsion should be done last
        if(password == passwordRepetition && password.length in 8..64) {
            if(isSuccessful) {
                this.password = password
            }
            else {
                this.password = ""
            }
        }
        else {
            alerts.add(Alert("Passwords are not between 8 and 64 characters or does not match!", Alert.Type.ERROR))
            this.password = ""
            isSuccessful = false
        }


    }
}
/**
<input class="form-check-input" type="checkbox" id="checkRobot" required>
 **/