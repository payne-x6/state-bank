package gov.liberland.statebank.forms

import gov.liberland.statebank.utils.Alert

open class Form {
    val alerts : MutableList<Alert> = mutableListOf()

    var isSuccessful : Boolean = true
}