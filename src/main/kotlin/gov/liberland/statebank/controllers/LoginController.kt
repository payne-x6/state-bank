package gov.liberland.statebank.controllers

import gov.liberland.statebank.controllers.abstraction.BaseController
import gov.liberland.statebank.utils.Alert
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.*


@Controller
@RequestMapping("/login")
class LoginController : BaseController() {
    override val title: String
        get() = "Login"

    @GetMapping
    fun doGetError(model: Model, @RequestParam request : Map<String,String>) : String {
        if(request["error"] != null) {
            model["alerts"] = listOf(Alert("Wrong username or password", Alert.Type.ERROR))
        }
        return "login"
    }

}