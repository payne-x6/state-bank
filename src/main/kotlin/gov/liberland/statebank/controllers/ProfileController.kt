package gov.liberland.statebank.controllers

import gov.liberland.statebank.controllers.abstraction.BankingTemplateController
import gov.liberland.statebank.controllers.abstraction.BaseController
import gov.liberland.statebank.utils.Navigation
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/profile")
class ProfileController : BankingTemplateController() {

    override val title: String
        get() = "Profile"

    override val navigation: List<Navigation>
        get() = listOf(
                Navigation("Home", "/"),
                Navigation("Profile", "/profile")
        )

    @GetMapping
    fun doGet(model: Model) : String {
        return "banking-profile"
    }

    @PostMapping
    fun doPost(model: Model) : String {
        return "banking-profile"
    }
}