package gov.liberland.statebank.controllers

import gov.liberland.statebank.controllers.abstraction.BaseController
import gov.liberland.statebank.forms.SignupForm
import gov.liberland.statebank.services.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/signup")
class SignupController : BaseController() {
    override val title: String
        get() = "Sign up"

    @Autowired
    private lateinit var userService : UserService


    @GetMapping
    fun doGet(model: Model) : String {
        return "signup"
    }

    @PostMapping
    fun doPost(model: Model, @RequestParam request : Map<String, String>) : String {
        val form = SignupForm(request)

        if(form.isSuccessful) {
            userService.registerUser(form)
        }

        model["form"] = form
        model["alerts"] = form.alerts

        return "signup"
    }
}

