package gov.liberland.statebank.controllers

import gov.liberland.statebank.controllers.abstraction.BankingTemplateController
import gov.liberland.statebank.model.repository.AccountRepository
import gov.liberland.statebank.model.table.Account
import gov.liberland.statebank.utils.Navigation
import gov.liberland.statebank.utils.fromRequestToBoolean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
@RequestMapping( "/banking/account/create")
class BankingAccountCreateController : BankingTemplateController() {

    override val title: String
        get() = "Create Banking Account"

    override val navigation: List<Navigation>
        get() = listOf(
                Navigation("Home", "/"),
                Navigation("Banking", "/banking"),
                Navigation("Banking Accounts", "/banking/accounts"),
                Navigation("Create Account", "/banking/account/add")
        )

    @Autowired
    private lateinit var accountRepository: AccountRepository


    @GetMapping
    fun doGet(model: Model) : String {

        return "banking-account-create"
    }

    @PostMapping
    fun doPost(model: Model, @RequestParam request : Map<String, String>) : String {
        val name = request["account-name"].orEmpty()
        val transparent = request["account-transparent"].fromRequestToBoolean(false)


        val acc = Account(name = name, transparent = transparent, user = userService.getThisUser())
        accountRepository.save(acc)

        return "banking-account-create"
    }
}