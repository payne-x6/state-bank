package gov.liberland.statebank.controllers

import gov.liberland.statebank.controllers.abstraction.BankingTemplateController
import gov.liberland.statebank.controllers.abstraction.BaseController
import gov.liberland.statebank.forms.SignupForm
import gov.liberland.statebank.model.repository.RoleRepository
import gov.liberland.statebank.model.repository.UserRepository
import gov.liberland.statebank.services.UserService
import gov.liberland.statebank.utils.Navigation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
@RequestMapping( "/administration/users/add")
class AdministrationUserAddController : BankingTemplateController() {

    override val title: String
        get() = "Add User"

    override val navigation: List<Navigation>
        get() = listOf(
                Navigation("Home", "/"),
                Navigation("Administration", "/administration"),
                Navigation("Users", "/administration/users"),
                Navigation("Add User", "/administration/users/add")
        )


    @GetMapping
    fun doGet(model: Model) : String {
        return "administration-user-add"
    }

    @PostMapping
    fun doPost(model: Model, @RequestParam request : Map<String, String>) : String {
        val form = SignupForm(request)

        if(form.isSuccessful) {
            userService.registerUser(form)
        }

        model["alerts"] = form.alerts
        model["form"] = form
        return "administration-user-add"
    }
}