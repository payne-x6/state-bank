package gov.liberland.statebank.controllers

import gov.liberland.statebank.controllers.abstraction.BankingTemplateController
import gov.liberland.statebank.services.BankingAccountService
import gov.liberland.statebank.utils.Navigation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/banking/accounts")
class BankingAccountsController : BankingTemplateController() {

    override val title: String
        get() = "Banking Accounts"

    override val navigation: List<Navigation>
        get() = listOf(
                Navigation("Home", "/"),
                Navigation("Banking", "/banking"),
                Navigation("Accounts", "/banking/accounts")
        )

    @Autowired
    private lateinit var bankingAccountService: BankingAccountService

    @GetMapping
    fun doGet(model: Model) : String {
        model["accounts"] = bankingAccountService.getBankingAccounts()
        return "banking-accounts"
    }

    @PostMapping
    fun doPost(model: Model) : String {

        return "banking-accounts"
    }
}