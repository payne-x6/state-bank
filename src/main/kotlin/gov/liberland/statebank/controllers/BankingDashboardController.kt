package gov.liberland.statebank.controllers

import gov.liberland.statebank.controllers.abstraction.BankingTemplateController
import gov.liberland.statebank.utils.Navigation
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/banking", "/banking/dashboard")
class BankingDashboardController : BankingTemplateController() {

    override val title: String
        get() = "Banking Dashboard"

    override val navigation: List<Navigation>
        get() = listOf(
                Navigation("Home", "/"),
                Navigation("Banking", "/banking")
        )

    @GetMapping
    fun doGet(model: Model) : String {
        return "banking-dashboard"
    }

    @PostMapping
    fun doPost(model: Model) : String {

        return "banking-dashboard"
    }
}