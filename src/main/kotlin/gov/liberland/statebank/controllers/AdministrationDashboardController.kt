package gov.liberland.statebank.controllers

import gov.liberland.statebank.controllers.abstraction.BankingTemplateController
import gov.liberland.statebank.model.repository.UserRepository
import gov.liberland.statebank.services.UserService
import gov.liberland.statebank.utils.Navigation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/administration", "/administration/dashboard")
class AdministrationDashboardController : BankingTemplateController() {

    override val title: String
        get() = "Administration Dashboard"

    override val navigation: List<Navigation>
        get() = listOf(
                    Navigation("Home", "/"),
                    Navigation("Administration", "/administration")
                )

    @GetMapping
    fun doGet(model: Model) : String {
        model["users-count"] = userService.countUsers()
        model["admins-count"] = userService.countAdmins()


        //val auth = SecurityContextHolder.getContext().authentication
        //model["user-name"] = auth.name

        return "administration-dashboard"
    }
}