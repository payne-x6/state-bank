package gov.liberland.statebank.controllers

import gov.liberland.statebank.controllers.abstraction.BaseController
import gov.liberland.statebank.model.repository.UserRepository
import gov.liberland.statebank.services.AuthorizationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/", "/home")
class WelcomePageController : BaseController() {
    override val title: String
        get() = "Homepage"

    @GetMapping
    fun doGet(model: Model) : String {
        return "welcome-page"
    }

    @PostMapping
    fun doPost(model: Model) : String {
        return "welcome-page"
    }
}