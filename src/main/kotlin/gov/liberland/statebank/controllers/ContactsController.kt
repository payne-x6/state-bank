package gov.liberland.statebank.controllers

import gov.liberland.statebank.controllers.abstraction.BaseController

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/contacts")
class ContactsController : BaseController() {
    override val title: String
        get() = "Contacts"


    @GetMapping
    fun doGet(model: Model) : String {
        return "contacts"
    }

    @PostMapping
    fun doPost(model: Model) : String {
        return "contacts"
    }
}