package gov.liberland.statebank.controllers

import gov.liberland.statebank.controllers.abstraction.BankingTemplateController
import gov.liberland.statebank.controllers.abstraction.BaseController
import gov.liberland.statebank.forms.SignupForm
import gov.liberland.statebank.model.repository.UserRepository
import gov.liberland.statebank.model.table.User
import gov.liberland.statebank.utils.Navigation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
@RequestMapping("/administration/users")
class AdministrationUsersController : BankingTemplateController() {
    override val title: String
        get() = "Users Administration"

    override val navigation: List<Navigation>
        get() = listOf(
            Navigation("Home", "/"),
            Navigation("Administration", "/administration"),
            Navigation("Users", "/administration/users")
        )

    @Autowired
    private lateinit var userRepository : UserRepository

    @GetMapping
    fun doGet(model: Model) : String {

        model["users"] = userRepository.findAll()

        return "administration-users"
    }
}