package gov.liberland.statebank.controllers

import gov.liberland.statebank.controllers.abstraction.BaseController
import gov.liberland.statebank.services.InstallService
import gov.liberland.statebank.utils.Alert
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/install")
class InstallController : BaseController() {
    override val title: String
        get() = "Installation"

    @Autowired
    private lateinit var installService : InstallService

    @GetMapping
    fun doGet(model: Model) : String {
        return if(installService.isInstalled) {
            "installed"
        }
        else {
            "install"
        }
    }

    @PostMapping
    fun doPost(model: Model, @RequestParam request : Map<String, String>) : String {
        if(!installService.isInstalled) { //Not installed
            //Not big validation becouse I decided to make totally free when choosing the password for superuser!
            //Superuser should be done by conscious user!
            if(!request["password"].isNullOrBlank() && request["password"] == request["passwordRepeat"]) {
                val password = request["password"]!!

                installService.initRoles()
                installService.addSuperuser(password)

                model["alerts"] = listOf(Alert("Superuser have been created!", Alert.Type.SUCCESS))
            }
            else {
                model["alerts"] = listOf(Alert("Passwords are not same!", Alert.Type.ERROR))
            }
        }
        else {
            return "installed"
        }

        return "install"
    }
}