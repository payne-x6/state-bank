package gov.liberland.statebank.controllers.abstraction

import gov.liberland.statebank.services.UserService
import gov.liberland.statebank.utils.Navigation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.ModelAttribute

abstract class BankingTemplateController : BaseController() {

    @Autowired
    protected lateinit var userService : UserService

    protected abstract val navigation : List<Navigation>

    @ModelAttribute("navigation")
    fun navigation() : List<Navigation> {
        return navigation
    }

    @ModelAttribute("username")
    fun username() : String {
        return userService.getThisUser().name
    }

}