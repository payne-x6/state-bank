package gov.liberland.statebank.controllers.abstraction

import gov.liberland.statebank.services.AuthorizationService
import gov.liberland.statebank.utils.security.Authorization
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.ModelAttribute


abstract class BaseController {
    @Autowired
    lateinit var authorizationService : AuthorizationService

    protected abstract val title : String

    @ModelAttribute("title")
    fun title() : String {
        return title
    }

    @ModelAttribute("authorization")
    fun authorization() : Authorization {
        return authorizationService.getAuthorizationToken()
    }

}