package gov.liberland.statebank.config

import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpStatus
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer


@Configuration
@EnableWebMvc
class MvcConfig : WebMvcConfigurer {

    override fun addResourceHandlers(registry : ResourceHandlerRegistry) {
        registry.addResourceHandler("/images/**")
                .addResourceLocations( "classpath:static/images/")
        registry.addResourceHandler("/styles/**")
                .addResourceLocations( "classpath:static/styles/")

    }
}